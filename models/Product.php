<?php

namespace app\models;

use yii\db\ActiveRecord;


/**
 * Product is the model behind the product table.
 */
class Product extends ActiveRecord
{
    public $image;
    public $is_deleted;

    public static function tableName()
    {
        return 'product';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // image is required
            [['image'], 'required'],
            ['is_deleted', 'integer'],
        ];
    }

    public function getStoreProduct()
    {
        return $this->hasOne(StoreProduct::class, ['product_id' => 'id']);
    }
}
