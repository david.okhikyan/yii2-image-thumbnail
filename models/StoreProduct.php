<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * StoreProduct is the model behind the store_product table.
 */
class StoreProduct extends ActiveRecord
{
    public $product_id;
    public $product_image;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // product_id,product_image are required
            [['product_id', 'product_image'], 'required']
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }
}
