<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%store_product}}`.
 */
class m200201_114751_create_store_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('store_product', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(11)->notNull(),
            'product_image' => $this->string(100)
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx-product-product_id',
            'store_product',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            'fk-product-product_id',
            'store_product',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops index for column `product_ip`
        $this->dropIndex(
            'idx-product-product_id',
            'store_product'
        );

        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk-product-product_id',
            'store_product'
        );

        $this->dropTable('store_product');
    }
}
