<?php

namespace app\utils;

use Imagine\Image\ImageInterface;
use yii\imagine\Image as ImagineImage;

/**
 * This is a class for creating image thumbnails.
 *
 * Class Image
 * @author Davit Okhikyan <david.okhikyan@gmail.com>
 * @package app\utils
 */
class Image
{
    /**
     * Create image thumbnail with given dimensions
     *
     * @param $imageLink
     * @param array $dimensions
     * @return ImageInterface
     * @throws \Exception
     *
     */
    public static function generateMiniature($imageLink, array $dimensions)
    {
        // check if the dimensions are passed otherwise set default values
        if (isset($dimensions["width"]) === false) {
            $dimensions["width"] = 200;
        } elseif (isset($dimensions["height"]) === false) {
            $dimensions["height"] = 200;
        }

        try {
            return ImagineImage::thumbnail($imageLink, $dimensions["width"], $dimensions["height"])->save(\Yii::getAlias("@runtime/thumbnails/{$imageLink}"));
        } catch (\Exception $e) {
            throw new \Exception("Error while creating an image thumbnail.");
        }
    }

    /**
     * Create image watermarked thumbnail
     *
     * @param $imageLink
     * @param $watermarkImage
     * @param array $dimensions
     * @return ImageInterface
     * @throws \Exception
     */
    public static function generateWatermarkedMiniature($imageLink, $watermarkImage, array $dimensions)
    {
        // check if the dimensions are passed otherwise set default values
        if (isset($dimensions["width"]) === false) {
            $dimensions["width"] = 200;
        } elseif (isset($dimensions["height"]) === false) {
            $dimensions["height"] = 200;
        }

        try {
            return ImagineImage::watermark(self::generateMiniature($imageLink, $dimensions), $watermarkImage)->save(\Yii::getAlias("@runtime/thumbnails/{$imageLink}"));
        } catch (\Exception $e) {
            throw new \Exception("Error while creating an image watermarked thumbnail.");
        }
    }
}
