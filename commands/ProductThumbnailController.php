<?php

namespace app\commands;

use app\models\Product;
use app\utils\Image;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Command to generate thumbnails for products
 *
 * Class ProductThumbnailController
 * @package app\commands
 */
class ProductThumbnailController extends Controller
{
    public $sizes;
    public $watermarked;
    public $catalogOnly;

    public function options($actionID)
    {
        return [
            'sizes',
            'watermarked',
            'catalogOnly'
        ];
    }

    public function optionAliases()
    {
        return [
            's' => 'sizes',
            'w' => 'watermarked',
            'co' => 'catalogOnly'
        ];
    }

    public function actionIndex()
    {
        // if sizes is not passed return an error message
        if (is_null($this->sizes)){
            $this->stdout("Please enter sizes (-s) option\n", Console::NEGATIVE);
            return 1;
        }
        if (is_null($this->watermarked)){
            $this->watermarked = false;
        }
        if (is_null($this->catalogOnly)){
            $this->catalogOnly = true;
        }

        // set initial success/fail counts
        $successfulThumbnailCount = 0;
        $failedThumbnailCount = 0;

        // products initial query
        $productsQuery = Product::find()->where(["is_deleted" => 0]);

        if ($this->catalogOnly){
            $products = $productsQuery->innerJoinWith('storeProduct')->all();
        }
        else{
            $products = $productsQuery->all();
        }

        $dimensions = explode(",", $this->sizes);

        foreach ($products as $product){
            foreach ($dimensions as $dimension){
                $dimensionParts = explode("x", $dimension);

                // if both width and height are passed
                if (count($dimensionParts) == 2){
                    $imageDimension = [
                        "width" => $dimensionParts[0],
                        "height" => $dimensionParts[1]
                    ];
                }
                else{
                    $imageDimension = [
                        "width" => $dimensionParts[0],
                        "height" => $dimensionParts[0]
                    ];
                }

                try{
                    if ($this->watermarked){
                        Image::generateWatermarkedMiniature($product["image"], "watermarkimagetoappendonimage.jpg", $imageDimension);
                    }
                    else{
                        Image::generateMiniature($product["image"], $imageDimension);
                    }

                    $successfulThumbnailCount++;
                }
                catch (\Exception $e){
                    $failedThumbnailCount++;
                }
            }
        }

        $this->stdout("Successful thumbnails created count is: {$successfulThumbnailCount}\n", Console::NEGATIVE);
        $this->stdout("Failed creating thumbnail count is : {$failedThumbnailCount}\n", Console::NEGATIVE);
        return 0;
    }
}
